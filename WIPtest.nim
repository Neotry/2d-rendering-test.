# WIPEnginetest.nim
{.link: "libs.o".} # glfw3.h, glew.h and stb_image.h linkage.

import math

type 
    OGLfloat = float32
    OGLuint = uint32
    OGLint = int32

const
    GLFW_CONTEXT_VERSION_MAJOR = 0x00022002
    GLFW_CONTEXT_VERSION_MINOR = 0x00022003
    GLFW_OPENGL_PROFILE = 0x00022008
    GLFW_OPENGL_CORE_PROFILE = 0x00032001

const 
    GL_COLOR_BUFFER_BIT = 0x00004000
    GL_DEPTH_BUFFER_BIT = 0x00000100
    GL_ACCUM_BUFFER_BIT = 0x00000200
    GL_STENCIL_BUFFER_BIT = 0x00000400
    GL_ARRAY_BUFFER = 0x8892
    GL_ELEMENT_ARRAY_BUFFER = 0x8893
    GL_FALSE = 0.char
    GL_STATIC_DRAW = 0x88E4
    GL_FLOAT = 0x1406
    GL_VERTEX_SHADER = 0x8B31
    GL_COMPILE_STATUS = 0x8B81
    GL_INFO_LOG_LENGTH = 0x8B84
    GL_FRAGMENT_SHADER = 0x8B30
    GL_LINK_STATUS = 0x8B82
    GL_TRIANGLES = 0x0004
    GL_UNSIGNED_INT= 0x1405
    GL_VERSION = 0x1F02
    GL_TEXTURE_2D = 0x0DE1
    GL_TEXTURE_WRAP_S = 0x2802
    GL_REPEAT = 0x2901
    GL_TEXTURE_WRAP_T = 0x2803
    GL_TEXTURE_MIN_FILTER = 0x2801
    GL_TEXTURE_MAG_FILTER = 0x2800
    GL_LINEAR = 0x2601
    GL_RGB = 0x1907
    GL_RGBA = 0x1908
    GL_BGR = 0x80E0
    GL_UNSIGNED_BYTE = 0x1401
    GL_BYTE = 0x1400
    GL_SRC_ALPHA = 0x0302
    GL_ONE_MINUS_SRC_ALPHA = 0x0303
    GL_BLEND = 0x0BE2
    GL_CLAMP_TO_EDGE = 0x812F

const 
    POSITION_LENGTH = 3.OGLint
    TEX_COORD_LENGTH  = 2.OGLint

const
    WINDOW_W = 640
    WINDOW_H = 480

type Mat4x4* = array[16, OGLfloat] # 4 x 4 Matrix for the 2D projection

let
    vertexLength = POSITION_LENGTH + TEX_COORD_LENGTH
    texcoordDataOffset = POSITION_LENGTH * sizeof(OGLfloat)

let identityMat4x4*:Mat4x4 = [OGLfloat(1), 0, 0, 0, 
                                       0, 1, 0, 0, 
                                       0, 0, 1, 0, 
                                       0, 0, 0, 1]

proc glfwInit():cint {.importc.}

proc glfwCreateWindow(width, height:int, title:cstring = "GLFW Window", monitor:pointer = nil,
    share:pointer = nil):pointer {.importc.}

proc glfwMakeContextCurrent(window:pointer) {.importc.}


proc glfwSwapBuffers(window:pointer) {.importc.}

proc glfwPollEvents() {.importc.}

proc glfwWindowShouldClose(window:pointer):cint {.importc.}

proc glfwDestroyWindow(window:pointer) {.importc.}

proc glfwTerminate() {.importc.}

proc glfwWindowHint(hint:int, value:int) {.importc.}

proc glewInit():char {.importc.}

proc glClearColor(r, g, b, a:float32) {.importc.}

proc glClear(mask:int) {.importc.}

proc glGenVertexArrays(n:OGLuint, arrays:ptr OGLuint) {.importc.}

proc glGenBuffers(n:OGLuint, buffers:ptr OGLuint) {.importc.}

proc glBindVertexArray(n:OGLuint) {.importc.}

proc glBindBuffer(target:OGLuint, buffer:OGLuint) {.importc.}

proc glBufferData(target:OGLuint, size:int, data:pointer, 
    usage:OGLuint) {.importc.}

proc glVertexAttribPointer(index:OGLuint, size:OGLint, glType:OGLuint, normalized:char, 
            stride:OGLint, glPtr:pointer) {.importc.}

proc glEnableVertexAttribArray(index:OGLuint) {.importc.}

proc glCreateShader(glType:OGLuint):OGLuint {.importc.}

proc glShaderSource(shd: OGLuint, count: OGLuint, src: ptr cstring, 
        length: ptr OGLint) {.importc.}

proc glCompileShader(shd: OGLuint) {.importc.}

proc glGetShaderiv(shd: OGLuint, pname: OGLuint, params: ptr OGLint) {.importc.}

proc glCreateProgram():OGLuint {.importc.}

proc glAttachShader(program:OGLuint, shader:OGLuint) {.importc.}

proc glLinkProgram(program:OGLuint) {.importc.}

proc glGetProgramiv(program:OGLuint, pname:OGLuint, params:ptr OGLint) {.importc.}

proc glDeleteShader(shd:OGLuint) {.importc.}

proc glDrawElements(mode:OGLuint, count:OGLint, glType:OGLuint, 
        indices: pointer) {.importc.}

proc glUseProgram(program:OGLuint) {.importc.}

proc glDeleteVertexArrays(n:OGLint, arrays:ptr OGLuint) {.importc.}

proc glDeleteBuffers(n:OGLint, buffers:ptr OGLuint) {.importc.}

proc glViewport(x: OGLint, y: OGLint, width: OGLuint, height: OGLuint) {.importc.}

proc glGetUniformLocation(program:OGLuint, name:cstring):OGLint {.importc.}

proc glUniformMatrix4fv(location:OGLint, count:OGLint, transpose:char,
                        value:ptr OGLfloat) {.importc.}

proc glGenTextures(n: OGLint, textures: ptr OGLuint) {.importc.}

proc glBindTexture(target: OGLuint, texture: OGLuint) {.importc.}

proc glTexParameteri(target: OGLuint, pname: OGLuint, param: OGLint) {.importc.}

proc glTexImage2D(target: OGLuint, level: OGLint, internalformat: OGLint, width: OGLint, height: OGLint, 
                  border: OGLint, format:OGLuint, `type`: OGLuint, pixels: pointer) {.importc.}

proc glBlendFunc(sfactor, dfactor:OGLuint) {.importc.}

proc glEnable(cap:OGLuint) {.importc.}

proc stbi_load(filename:cstring, x, y:ptr int, comp:ptr int, req_comp:int):seq[char] {.importc.}

###############################################################################

var 
    vertices = @[OGLfloat(0), 0, 0, # Position   
                                0, 0, # Texcoords
                          225, 0, 0,
                                1.0, 0.0,
                          225, 225, 0,
                                1.0, 1.0,
                          0, 225, 0,
                                0.0, 1.0
                          ]

    indices = @[OGLuint(0), 1, 2, 2, 3, 0]

proc orthoProjection*(left, right, bottom, top, far, near:float):Mat4x4 =
    result = identityMat4x4
    result[0] = OGLfloat(2.0 / (right - left))
    result[5] = OGLfloat(2.0 / (top - bottom))
    result[3] = OGLfloat(- ((right + left) / (right - left)))
    result[7] = OGLfloat(- ((top + bottom) / (top - bottom)))
    result[10] = OGLfloat(-2 / (far - near))
    result[11] = OGLfloat(-((far + near)/(far - near)))
 

var orthoProjMtx = orthoProjection(0.0, WINDOW_W, WINDOW_H, 0.0, - 1.0, 1.0)

var glfwErr = glfwInit()
glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3)
glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3)
glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)
var winHandle = glfwCreateWindow(WINDOW_W, WINDOW_H)
glfwMakeContextCurrent(winHandle)
var glewErr = glewInit()

var
    shadID:OGLuint
    vertSrc:cstring = """
        #version 330 core

        layout (location = 0) in vec3 aPos;
        layout (location = 1) in vec2 aTexCoord;

        out vec2 texCoord;
        uniform mat4 projection;
        
        void main()
        {
            gl_Position = projection * vec4(aPos, 1.0f);
            texCoord = aTexCoord;
        }
        """
    fragSrc:cstring = """
        #version 330 core

        out vec4 FragColor;
        in vec2 texCoord;

        uniform sampler2D matTexture;

        void main()
        {
            FragColor = texture(matTexture, texCoord);
        }

        """

var success:OGLint
# vertex
var vertexShader = glCreateShader(GL_VERTEX_SHADER)
glShaderSource(vertexShader, 1, addr vertSrc, nil)
glCompileShader(vertexShader)
# Check compilation errors.
glGetShaderiv(vertexShader, GL_COMPILE_STATUS, addr success)
if bool(success) == false:
    echo(" vertex shader compilation failed")
    
# fragment
var fragmentShader = glCreateShader(GL_FRAGMENT_SHADER)
glShaderSource(fragmentShader, 1, addr fragSrc, nil)
glCompileShader(fragmentShader)
# Check compilation errors.
glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, addr success)
if bool(success) == false:
    echo("fragment shader compilation failed")

# Shader program
shadID = glCreateProgram()
glAttachShader(shadID, vertexShader)
glAttachShader(shadID, fragmentShader)
glLinkProgram(shadID)
# Check for linkage errors.
glGetProgramiv(shadID, GL_LINK_STATUS, addr success)
if success == 0:
    echo("program linking failed") 
glDeleteShader(vertexShader)
glDeleteShader(fragmentShader)

glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
glEnable(GL_BLEND)

var 
    textureID:OGLuint
    texWidth:OGLint
    texHeight:OGLint
    channelCount:OGLint
    imageData = stbi_load("awesome_face.png", addr texWidth, addr texHeight, addr channelCount, 0)

#texture
glGenTextures(1, addr textureID)
glBindTexture(GL_TEXTURE_2D, textureID)
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, addr imageData[0])

glViewport(0, 0, WINDOW_W, WINDOW_H)

var VAO, VBO, EBO:OGLuint
glGenVertexArrays(1, addr VAO)
glGenBuffers(1, addr VBO)
glGenBuffers(1, addr EBO)
glBindVertexArray(VAO)
glBindBuffer(GL_ARRAY_BUFFER, VBO)
glBufferData(GL_ARRAY_BUFFER, vertices.len * sizeof(OGLfloat), 
             addr vertices[0], GL_STATIC_DRAW)
glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO)
glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.len * sizeof(OGLuint), 
             addr indices[0], GL_STATIC_DRAW)
# Position layout
glVertexAttribPointer(0, POSITION_LENGTH, GL_FLOAT, GL_FALSE, OGLint(vertexLength * sizeof(OGLfloat)), 
                      nil)
glEnableVertexAttribArray(0)

# Texcoord layout
glVertexAttribPointer(1, TEX_COORD_LENGTH, GL_FLOAT, GL_FALSE, OGLint(vertexLength * sizeof(OGLfloat)), 
                      cast[pointer](texcoordDataOffset))
glEnableVertexAttribArray(1)

glBindBuffer(GL_ARRAY_BUFFER, 0)
glBindVertexArray(0)
glUseProgram(shadID)

while bool(glfwWindowShouldClose(winHandle)) == false:
    glClearColor(0.2, 0.3, 0.3, 1.0)
    glClear(GL_COLOR_BUFFER_BIT)
    glBindVertexArray(VAO)
    glUniformMatrix4fv(glGetUniformLocation(shadID, "projection"), 1, char(true), addr orthoProjMtx[0])
    glBindTexture(GL_TEXTURE_2D, textureID)
    glDrawElements(GL_TRIANGLES, OGLint(indices.len), GL_UNSIGNED_INT, nil)
    glfwSwapBuffers(winHandle)
    glfwPollEvents()

glDeleteVertexArrays(1, addr VAO)
glDeleteBuffers(1, addr VBO)
glDeleteBuffers(1, addr EBO)
glfwDestroyWindow(winHandle)
glfwTerminate()
